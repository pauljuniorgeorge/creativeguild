<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
                                  
    <title>CreativeGuild · TechTest</title>


    <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
   
   <style>
   
   .infoWrapper{
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 800px;
    text-align: center;

   }

   h2{
       color:red;
   }

   h3{
       color:#ccc;
   }
   
   </style>
   
   </head>
  <body>

<div class="infoWrapper">
<h2>Welcome</h2>

<h3>
To view a user navigate to {SiteRoot}/user/user_id
</h3>

<h3>
To acces via api make requests to {SiteRoot}/api/user/user_id
</h3>
</div>


</body>
</html>