<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
                                  
    <title>CreativeGuild · TechTest</title>


    <script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  



    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      html {
        font-size: 14px;
      }
      @media (min-width: 768px) {
      html {
        font-size: 16px;
        }
      }

      .container {
        max-width: 950px;
      }

      .gallery-header {
        max-width: 920px;
        border:#ccc 1px solid;
        border-radius: 10px;
        margin-top: 30px;
        margin-bottom: 30px;
      }

      .card-deck .card {
        min-width: 220px;
      }

      .profile-image{
        width:150px;
        height:150px;
        border-radius:100px;
      }

      .userInfoWrapper{
        padding:25px;
      }

      .userNameBio{
        padding-left:45px;
        padding-top:10px;
      }

      .userInfoValue{
        font-size:12px;
        color: pink;
        font-weight: bold;
      }

      .userContactInfo{
        padding-left: 5px;
        padding-top: 30px;
      }

      @media (max-width: 768px) {
        .userNameBio{
        padding-left:0;
        text-align: center;
      }
      .profile-image{
       margin-left:auto;
       margin-right:auto;
       display:block;
      }

      .gallery-header{
       border:none;
      }

      }
      .galleryTitle{
        position: relative;
        font-weight: bold;
        color: white;
        top: -35px;
        text-align: left;
        padding-left: 15px;
      }

      .loader{
        display: block;
        margin-left: auto;
        margin-right: auto;
        margin-top: 400px;
      }
     
    </style>

<script>

$.get("/api/user/{{$id}}?view=userData", function(data){
    $("body").html(data);
  });

</script>

  </head>
  <body>
    
<img class="loader" src="/img/loader.gif" />

</body>
</html>