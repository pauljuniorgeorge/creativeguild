<div class="container gallery-header">
<div class="row userInfoWrapper">
    <div class="col col-lg-2 col-12">
      <img src="{{$user->profile_picture}}" class="profile-image" />
    </div>
    <div class="col col-lg-8 col-12 userNameBio">
      <h3>{{$user->name}}</h3>
      <h6>Bio</h6>
      <span style="color:#ccc;font-size:12px;">{{$user->bio}}</span>
    </div>
    <div class="col col-lg-2 col-12 userContactInfo">
     <div class="userInfoTitle"><h6>Phone</h6></div>
     <div class="userInfoValue">{{$user->phone}}</div>
     <div class="userInfoTitle"><h6>Email</h6></div>
     <div class="userInfoValue">{{$user->email}}</div>
    </div>
  </div>
  </div>
  
<div class="container">
  <div class="card-deck mb-3 text-center">
@foreach ($user->albums as $album)
<div class="card mb-4 shadow-sm">
            <img class="bd-placeholder-img card-img-top" width="100%" height="225" src="{{$album->cover_image}}" />            
            <span class="galleryTitle">{{$album->title}}</span>
            <div class="card-body">
              <p class="card-text">{{$album->description}}</p>
              <div class="d-flex justify-content-between align-items-center">
              @if ($album->featured == true)
              <svg title="featured" width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-heart-fill" fill="red" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
              </svg>
              @else
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-heart" fill="grey" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
              </svg>
              @endif         
                <small class="text-muted">{{ date('d-M-y', strtotime($album->date)) }}</small>
              </div>
            </div>
          </div>
@endforeach
  </div>
</div>



