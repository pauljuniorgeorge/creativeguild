<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class InitialContent extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_id = DB::table('users')->insertGetId([
            'name' => "Nick Reynolds",
            'email' => 'nick.reynolds@domain.co',
            'password' => Hash::make('password'),
            'bio' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
            'profile_picture' => '/img/profile.jpeg',
            'phone' => '555-555-5555'
        ]);

        DB::table('albums')->insert([
            'title' => "Nandhaka Pieris",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'cover_image' => '/img/landscape1.jpeg',
            'date' => '2015-05-01',
            'featured' => true,
            'user_id' => $user_id
        ]);

        DB::table('albums')->insert([
            'title' => "New West Calgary",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'cover_image' => '/img/landscape2.jpeg',
            'date' => '2016-05-01',
            'featured' => false,
            'user_id' => $user_id
        ]);

        DB::table('albums')->insert([
            'title' => "Australian Landscape",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'cover_image' => '/img/landscape3.jpeg',
            'date' => '2015-02-02',
            'featured' => false,
            'user_id' => $user_id
        ]);

        DB::table('albums')->insert([
            'title' => "Halvergate Marsh",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'cover_image' => '/img/landscape4.jpeg',
            'date' => '2014-04-01',
            'featured' => true,
            'user_id' => $user_id
        ]);

        DB::table('albums')->insert([
            'title' => "Rikkis Landscape",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'cover_image' => '/img/landscape5.jpeg',
            'date' => '2010-09-01',
            'featured' => false,
            'user_id' => $user_id
        ]);

        DB::table('albums')->insert([
            'title' => "Kiddi Kristjans Iceland",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'cover_image' => '/img/landscape6.jpeg',
            'date' => '2015-07-21',
            'featured' => true,
            'user_id' => $user_id
        ]);


    }
}
